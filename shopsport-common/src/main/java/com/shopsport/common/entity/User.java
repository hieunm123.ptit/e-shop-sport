package com.shopsport.common.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Objects;

import static com.shopsport.common.entity.constant.Constant.CommonConstants.AWS_S3_BUCKET_URL;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
@EqualsAndHashCode(callSuper = true)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@Slf4j
public class User extends BaseEntityWithUpdater implements Serializable {

  @Column(length = 128, nullable = false, unique = true)
  private String email;

  @Column(length = 64, nullable = false)
  private String password;

  @Column(length = 45, nullable = false)
  private String firstName;

  @Column(length = 45, nullable = false)
  private String lastName;

  @Column(length = 64)
  private String photos;

  private boolean enabled;

  @Column(name = "is_deleted")
  private boolean isDeleted;

  @ManyToOne
  @JoinColumn(name = "role_id", nullable = false)
  private Role role;

  public User(String email, String password, String firstName, String lastName) {
    this.email = email;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  @Transient
  public String getPhotosImagePath() {
    log.info("getPhotosImagePath() - photos: {}, id: {}", photos, id);

    if (Objects.isNull(photos) || Objects.isNull(id)) return "/images/default-user.png";
    return AWS_S3_BUCKET_URL + "user-photos/" + this.id + "/" + this.photos;
  }

  @Transient
  public String getFullName() {
    return firstName + " " + lastName;
  }

}
