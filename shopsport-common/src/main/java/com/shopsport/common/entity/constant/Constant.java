package com.shopsport.common.entity.constant;

public class Constant {

  private Constant() {
  }

  public static class CommonConstants {

    private CommonConstants() {
    }

    public static final String AWS_S3_BUCKET_URL = "https://linh-files.s3.us-east-2.amazonaws.com/";
  }
}
