package com.shopsport.server.utils;

import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListObjectsRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsResponse;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.S3Object;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

@Slf4j
public class AmazonS3Util {

  private AmazonS3Util() {
  }

  private static final String awsAccessKeyId = "AKIA5FTZBEFMLTBDSELT";

  private static final String awsSecretKey = "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";

  private static final String region = "us-east-2";

  private static S3Client createS3Client() {
    AwsBasicCredentials awsCreds = AwsBasicCredentials.create(awsAccessKeyId, awsSecretKey);
    return S3Client.builder()
          .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
          .region(Region.of(region))
          .build();
  }

  public static List<String> listFolder(String folderName) {
    S3Client s3Client = createS3Client();

    ListObjectsRequest listRequest = ListObjectsRequest.builder()
          .bucket("linh-files")
          .prefix(folderName)
          .build();

    ListObjectsResponse response = s3Client.listObjects(listRequest);

    List<S3Object> contents = response.contents();


    ListIterator<S3Object> iterator = contents.listIterator();

    List<String> listKeys = new ArrayList<>();


    while (iterator.hasNext()) {
      S3Object object = iterator.next();
      listKeys.add(object.key());
    }
    return listKeys;
  }

  public static void uploadFile(String folderName, String fileName, InputStream inputStream) {

    log.info("folderName: " + folderName);

    S3Client s3Client = createS3Client();

    PutObjectRequest putObjectRequest = PutObjectRequest.builder()
          .bucket("linh-files")
          .key(folderName + "/" + fileName)
          .acl("public-read")
          .build();

    try {
      int contentLength = inputStream.available();
      s3Client.putObject(putObjectRequest, RequestBody.fromInputStream(inputStream, contentLength));
    } catch (Exception e) {
      log.error("Error upload file to S3: " + e.getMessage());
    }
  }

  public static void deleteFile(String folderName, String fileName) {
    S3Client s3Client = createS3Client();

    s3Client.deleteObject(builder -> builder.bucket("linh-files").key(folderName + "/" + fileName));
  }

  public static void removeFolder(String folderName) {
    S3Client s3Client = createS3Client();

    List<String> listKeys = listFolder(folderName);

    for (String key : listKeys) {
      s3Client.deleteObject(builder -> builder.bucket("linh-files").key(key));
    }
  }
}
